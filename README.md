# README #

Son 3 proyectos independientes en el repositorio.

Hay un tag por cada paso en el curso (crear autoload, PSR, extract method, rename variable,...)

## 01 Refactoring
En este repositorio se enseñan técnicas básicas de refactoring, además se incluye parte específica de PHP como autoload, uso de composer y PSR

## 02 Testing
Uno de las mayores dificultades a la hora de refactorizar es testear antes de tocar nada. En este ejercicio vemos como empezar a testear una clase para "salvar" las dependencias de la clase que queremos testear

## 03 More Refactoring
Es un cliente PHP que ataca a la API [the cat api](http://thecatapi.com/) Esta kata no está solucionada. La es solo solucionar una parte y dejar la otra como reto del curso